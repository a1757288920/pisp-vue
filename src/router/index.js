import Vue from 'vue'
import Router from 'vue-router'
import Login from '../components/Login.vue';
import Home from '../components/Home.vue';
import Index from '../components/function/Index.vue';
import Blog from '../components/function/Blog.vue';
import Code from '../components/function/Code.vue';
import Download from '../components/function/Download.vue';
import Category from '../components/function/Category.vue';
import Show from '../components/function/Show.vue';
import Create from '../components/function/Create.vue';
import httpRequest from '@/utils/httpRequest'
import Homepage from '../components/function/Homepage.vue';
import OtherHomePage from '../components/function/OtherHomePage.vue';
import Edit from '../components/function/Edit.vue';
import Upload from '../components/function/Upload.vue';
import EditCodeAssist from '../components/function/EditCodeAssist.vue';
// import SingleUpload from '../components/function/SingleUpload.vue';

Vue.use(Router)

Vue.prototype.$http = httpRequest

const router = new Router({
    routes: [
        { path: '/', redirect: '/login' },
        { path: '/login', component: Login },
        {
            path: '/home',
            component: Home,
            redirect: '/index',
            children: [
                { path: '/index', component: Index },
                { path: '/blog', component: Blog },
                { path: '/code', component: Code },
                { path: '/download', component: Download },
                { path: '/upload', component: Upload },
                { path: '/category', component: Category },
                { path: '/show', component: Show },
                { path: '/create', component: Create },
                { path: '/homepage', component: Homepage },
                { path: '/otherhomepage', component: OtherHomePage },
                { path: '/edit', component: Edit },
                { path: '/editcodeassist', component: EditCodeAssist },
            ]
        },

    ]
})

router.beforeEach((to, from, next) => {
    if (to.path === '/login') return next();
    const tokenStr = window.sessionStorage.getItem('token')
    if (!tokenStr) return next('/login')
    next()
})

export default router