// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
// 导入全局样式表
import './assets/css/global.css'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import axios from 'axios';
import mavonEditor from 'mavon-editor';
// import editormd from 'editor.md';
import 'mavon-editor/dist/css/index.css'
import './plugins/hljs'

axios.defaults.baseURL = 'http://localhost:88/api/'
axios.interceptors.request.use(config => {
    config.headers.token = window.sessionStorage.getItem('token')
    return config
})
Vue.prototype.$http = axios
Vue.prototype.$axios = axios;
Vue.config.productionTip = false

Vue.use(ElementUI)
Vue.use(mavonEditor)
    // Vue.use($)
    // Vue.use(editormd)

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>'
})